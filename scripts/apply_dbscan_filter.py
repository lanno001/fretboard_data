import os, sys, argparse
from os.path import splitext, basename
import numpy as np
from sklearn.cluster import DBSCAN
import pandas as pd
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(__location__)
from helper_functions import parse_input_path, parse_output_dir

def bg_filter_trace(tr, eps, f_aex=None):
    if np.isnan(eps):
        if f_aex is None:
            return np.copy(tr)
        return tr, f_aex
    min_clust = 10
    if f_aex is not None:
        tr_joined = np.concatenate((tr, f_aex))
    else:
        tr_joined = tr
    pc10 = np.percentile(tr_joined, 20)
    tr_pc10 = tr_joined[tr_joined < pc10]
    clust = DBSCAN(eps=eps, min_samples=min_clust).fit(tr_pc10.reshape(-1, 1)).labels_
    if np.sum(np.unique(clust) != -1) == 0:
        bg = np.min(tr_joined)
    else:
        med_list = np.array([np.median(tr_pc10[clust == lab]) for lab in np.unique(clust)])
        bg = np.min(med_list)
    if f_aex is None:
        return tr - bg
    return tr - bg, f_aex - bg


parser = argparse.ArgumentParser(description='Apply DBSCAN filter and save traces. Used to generate data for tools'
                                             'that do not have a background intensity filter.')
parser.add_argument('--in-dir', type=str, required=True)
parser.add_argument('--eps', type=float, default=15)
parser.add_argument('--add-fret-trace', action='store_true')
parser.add_argument('--out-dir', type=str, required=True)
args = parser.parse_args()

dat_list = parse_input_path(args.in_dir, pattern='*.dat')
out_dir = parse_output_dir(args.out_dir)

for dat_fn in dat_list:
    original_df = pd.read_csv(dat_fn, header=None, names=['time', 'f_dex_dem_raw', 'f_dex_aem_raw'], sep='\t')
    original_df.f_dex_dem_raw = bg_filter_trace(original_df.f_dex_dem_raw.to_numpy(), eps=args.eps)
    original_df.f_dex_aem_raw = bg_filter_trace(original_df.f_dex_aem_raw.to_numpy(), eps=args.eps)
    if args.add_fret_trace:
        original_df.loc[:, 'e_fret'] = original_df.f_dex_aem_raw / (original_df.f_dex_aem_raw + original_df.f_dex_dem_raw)
        original_df.fillna(0, inplace=True)
        # original_df.drop(['f_dex_aem_raw'], axis=1, inplace=True)
    original_df.to_csv(f'{out_dir}{basename(dat_fn)}', header=False, index=False, sep='\t')
