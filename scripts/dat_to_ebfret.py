import os, sys, argparse
from os.path import splitext, basename
import numpy as np
import pandas as pd
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(__location__)
from helper_functions import parse_input_path, parse_output_dir

parser = argparse.ArgumentParser(description='Convert multiple dats in FRETboard format'
                                             ' to single file of stacked dat format read by ebFRET')
parser.add_argument('--in-dir', type=str, required=True)
parser.add_argument('--out-dir', type=str, required=True)
args = parser.parse_args()

dat_fn_list = parse_input_path(args.in_dir, pattern='*.dat')
out_dir = parse_output_dir(args.out_dir)
df_list = []
num2fn_df = pd.DataFrame(index=np.arange(1, len(dat_fn_list)+1), columns=['dat_fn'])

for dit, dat_fn in enumerate(dat_fn_list):
    num2fn_df.loc[dit+1] = basename(dat_fn)
    with open(dat_fn, 'r') as fh:
        dat_array = np.array([line.strip().split('\t')[1:] for line in fh.readlines()]).astype(float)
    df_list.append(pd.DataFrame({'label': dit+1,
                                 'D':dat_array[:,0], 'A': dat_array[:, 1]}))
df = pd.concat(df_list)
df.to_csv(f'{out_dir}ebFRET_stacked.dat', header=False, index=False, sep=' ')
num2fn_df.to_csv(f'{out_dir}ebFRET_numdict.csv', header=True, index=True)
