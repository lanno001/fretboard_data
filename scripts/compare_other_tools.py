import os, sys, argparse, re
from io import StringIO
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from os.path import basename
import numpy as np
import pandas as pd
from scipy.linalg import logm
from itertools import permutations, chain
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(__location__)
from helper_functions import parse_input_path, parse_output_dir

from sklearn.cluster import KMeans

colors = ['#1b9e77', '#d95f02', '#7570b3', '#e7298a', '#66a61e']

plt.rcParams.update({'font.size': 22})

def plot_tool_tr_bars(df, ax, color_dict):
    manual_df = df.query('tool == "manual"').copy()
    plot_df = df.query('tool != "manual"').copy()
    tool_list = list(plot_df.tool.unique())
    tool_list.sort()
    tool_list.remove('FRETboard')
    tool_list = tool_list + ['FRETboard']
    nb_tools = len(tool_list)
    tridx_dict = {tr: it for it, tr in enumerate(plot_df.transition.unique())}
    plot_dists = np.linspace(-1 * (nb_tools // 2), nb_tools // 2, nb_tools) * 0.1
    tn_dict = {tn: plot_dists[it] for it, tn in enumerate(tool_list)}
    plot_df.loc[:, 'x'] = plot_df.apply(lambda x: tridx_dict[x.transition] + tn_dict[x.tool], axis=1)
    # color_dict = {tn: colors[it] for it, tn in enumerate(tool_list)}
    plot_df.loc[:, 'color'] = plot_df.tool.apply(lambda x: color_dict[x])

    manual_df.loc[:, 'lb'] = manual_df.transition.apply(lambda x: tridx_dict[x] + plot_dists[0] - 0.2)
    manual_df.loc[:, 'rb'] = manual_df.transition.apply(lambda x: tridx_dict[x] + plot_dists[-1] + 0.2)
    # manual_df.loc[:, 'lb'] = plot_dists[0] + np.arange(len(manual_df)) - 0.05
    # manual_df.loc[:, 'rb'] = plot_dists[-1] + np.arange(len(manual_df)) + 0.05

    # fig, ax = plt.subplots(nrows=1)
    ax.errorbar(x=plot_df.x, y=plot_df.tr,
                yerr=plot_df.loc[:, ('ci_low', 'ci_high')].to_numpy().T,
                color='white',
                ecolor=plot_df.color, fmt='.')
    ax.scatter(x=plot_df.x, y=plot_df.tr, color=plot_df.color)
    ax.hlines(y='tr', xmin='lb', xmax='rb', linestyles='dashed', colors='black', data=manual_df)
    ax.set_ylabel('transition rate ($s^{-1}$)')
    ax.set_xlabel('transition')
    ax.set_xticks(ticks=list(range(len(tridx_dict))))
    ax.set_xticklabels(labels=[tr.replace('_', '') for tr in tridx_dict])

def plot_efret_dists(df, ax, color_dict, style='box'):
    """
    Plot E_FRET distributions in histograms or boxplots. If Histograms, [ax] should be a list of
    axes with len(ax) == nb states.
    """
    df = df.loc[np.invert(np.logical_or(np.abs(df.efret) == np.inf, df.efret.isna())), :].copy()
    manual_df = df.query('tool == "manual"').copy()
    plot_df = df.query('tool != "manual"').copy()
    tool_list = plot_df.tool.unique()
    tool_list.sort()
    tool_list = list(tool_list[tool_list != 'FRETboard']) + ['FRETboard']
    c_list = [color_dict[tool] for tool in tool_list]
    if style == 'box':
        c_list = c_list + ['#ffffff']
        tool_list = tool_list + ['manual']
        sns.boxplot(x='state', y='efret', hue='tool',
                    hue_order=tool_list, palette=sns.color_palette(c_list),
                    data=df,
                    width=0.3, showfliers=False, ax=ax)
        ax.set_ylim((-1, 2))
        ax.set_xlabel('')
        ax.set_ylabel('$E_{PR}$')
    elif style == 'hist':
        states = df.state.unique()
        for ax_id, st in enumerate(states):
            if ax_id >= len(ax): break
            sns.histplot(x='efret', color='grey', data=manual_df.query(f'state == {st}'),
                         stat='count', bins=np.arange(0,1,0.01),
                         element='step', fill=True, ax=ax[ax_id]
                         )
            sns.histplot(x='efret', hue='tool', data=plot_df.query(f'state == {st}'),
                         stat='count', bins=np.arange(0,1,0.01), palette=sns.color_palette(c_list),
                         element='step', fill=False, ax=ax[ax_id]
                         )
            ax[ax_id].set_xlim((0,1))
            ax[ax_id].set_xlabel('$E_{PR}$')


def parse_fretboard_results(in_dir, soi):
    # --- parse transition rates ---
    ci_limits = np.load(f'{in_dir}/summary_stats/transitions.tsv.npy')[0, :, :].reshape(-1)
    trdf = pd.read_csv(f'{in_dir}/summary_stats/transitions.tsv', sep='\t', header=0, names=['FRETboard', 'manual'])
    trdf = trdf.reset_index().rename({'index': 'transition'}, axis=1).melt(value_vars=['FRETboard', 'manual'],
                                                                           id_vars=['transition'], var_name=['tool'],
                                                                           value_name='tr')
    trdf.loc[trdf.tool == 'FRETboard', 'ci_low'] = ci_limits[:len(ci_limits) // 2]
    trdf.loc[trdf.tool == 'FRETboard', 'ci_high'] = ci_limits[len(ci_limits) // 2:]
    trdf.loc[trdf.tool == 'manual', ('ci_low', 'ci_high')] = 0.0

    # --- parse efret values ---
    efdf_list = []
    efdf_manual_list = []
    fn_list = parse_input_path(f'{in_dir}/trace_csvs', pattern='*.csv')
    for fn in fn_list:
        trace_df = pd.read_csv(fn, sep='\t')
        efdf_list.append(trace_df.loc[:, ('E_FRET', 'predicted')].copy())
        efdf_manual_list.append(trace_df.loc[:, ('E_FRET', 'manual')].copy())
    efdf = pd.concat(efdf_list)
    efdf.rename({'E_FRET': 'efret', 'predicted': 'state'}, axis=1, inplace=True)
    efdf.loc[:, 'tool'] = 'FRETboard'
    efdf_manual = pd.concat(efdf_manual_list)
    efdf_manual.rename({'E_FRET': 'efret', 'manual': 'state'}, axis=1, inplace=True)
    efdf_manual.loc[:, 'tool'] = 'manual'
    efdf = pd.concat((efdf, efdf_manual))

    efdf = efdf.query(f'state in {str(soi)}').copy()
    # # todo Quick fix: remove trash states
    # states_of_interest = set(chain.from_iterable([tr.split('_') for tr in trdf.transition.unique()]))
    # states_of_interest = [int(s) for s in states_of_interest]
    # efdf = efdf.query(f'state in {str(states_of_interest)}')

    return efdf, trdf


def parse_ebfret_results(in_dir, framerate, soi):

    # --- parse original summary ---
    with open(f'{in_dir}/ebFRET_analyzed_summary.csv') as fh:
        eb_txt = fh.read()
    eb_params_txt = re.search('Parameters[\sa-zA-Z.,0-9_+-]+', eb_txt).group(0)
    nb_states = int(re.search('(?<=Num_States,)[0-9]+', eb_params_txt).group(0))
    tr_names = [f'{tr[0]}_{tr[1]}' for tr in permutations(np.arange(1, nb_states + 1), 2)]

    # eb_means_txt = re.search('(?<=Center)[\sa-zA-Z.,0-9_+-]+(?=Precision)', eb_params_txt).group(0).strip().replace(' ', '')
    # efret_df = pd.read_csv(StringIO(eb_means_txt), names=list(range(1,nb_states + 1))).T
    # efret_dict[exp]['ebFRET'] = {ri: {'mean': r.loc['Mean'], 'sd': r.loc['Std']} for ri, r in efret_df.iterrows()}

    eb_trans_txt = re.search('(?<=Transition_Matrix)[\sa-zA-Z.,0-9_+-]+', eb_params_txt).group(0).strip().replace(' ', '')
    eb_trans_means_txt = re.search('(?<=Mean,)[\sa-zA-Z.,0-9_+-]+(?=Std)', eb_trans_txt).group(0).replace('\n,', '\n')
    transition_probs = np.genfromtxt(StringIO(eb_trans_means_txt), delimiter=',')
    transition_rates = (np.eye(nb_states) + framerate * logm(transition_probs))[np.invert(np.eye(nb_states, dtype=bool))]
    # transition_rates = np.genfromtxt(StringIO(eb_trans_means_txt), delimiter=',')[np.invert(np.eye(nb_states, dtype=bool))]
    trdf = pd.DataFrame({'transition': tr_names, 'tool': 'ebFRET', 'tr': transition_rates}).set_index(['transition'])

    # --- parse bootstrap summary ---
    with open(f'{in_dir}/ebFRET_analyzed_summary_bootstrapped.csv') as fh:
        eb_bs_txt = fh.read()
    tr_bs_txt = re.search('(?<=bootstrap_tr\n\s{4}tr)[\sa-zA-Z.,0-9_+-]+', eb_bs_txt).group(0)
    bs_df = pd.read_csv(StringIO(tr_bs_txt), names=[f'{tr[0]}_{tr[1]}' for tr in permutations(np.arange(1, nb_states + 1), 2)])  # order of columns was accidentally reversed for bootstrap script!
    for cn in bs_df.columns:
        cur_mean, cur_sd = bs_df.loc[:, cn].mean(), bs_df.loc[:, cn].std()
        ci_low, ci_high = cur_mean - 2 * cur_sd, cur_mean + 2 * cur_sd
        trdf.loc[cn, 'ci_low'] = trdf.loc[cn, 'tr'] - ci_low
        trdf.loc[cn, 'ci_high'] = ci_high - trdf.loc[cn, 'tr']
    trdf.reset_index(inplace=True)

    # --- parse classified traces ---
    edf = pd.read_csv(f'{in_dir}/ebFRET_analyzed.dat', sep='\s+', names=['trace_nb', 'd', 'a', 'state'])
    edf.state = edf.state.astype(int)
    edf = edf.query(f'state in {str(soi)}').copy()
    edf.loc[:, 'efret'] = edf.a / (edf.d + edf.a)
    edf.drop(['d', 'a', 'trace_nb'], inplace=True, axis=1)
    edf.loc[:, 'tool'] = 'ebFRET'
    return edf, trdf

def parse_mashfret_results(in_dir, soi):
    """
    Parse contents of MASH-FRET results directory. Special attention given to:
    - kinetic fingerprinting experiment: transition rates need to be separated out
    - 3-state E_FRET difference experiment: stasi results need a clustering step
    """
    # parse transition rates
    fit_fn_list = parse_input_path(f'{in_dir}/kinetics', pattern='*.fit')
    fit_dict = {tuple(int(a) for a in re.search('[0-9]+to[0-9]+', fit_fn).group(0).split('to')): fit_fn for fit_fn in
                fit_fn_list}
    unique_fret_values = np.unique(np.array(list(fit_dict)).reshape(-1))
    unique_fret_values.sort()
    efret2num_dict = {ufv: i + 1 for i, ufv in enumerate(unique_fret_values)}  # fret states sorted low to high
    tr_indices = list(permutations(unique_fret_values, 2))
    if exp == '4_kinetic_fingerprint':
        tr_indices = list(chain.from_iterable([[list(tr) + [0], list(tr) + [1]] for tr in tr_indices]))
    dt_df = pd.DataFrame(0.0, index=pd.MultiIndex.from_tuples(tr_indices),
                         columns=['mean', 'sd'])

    for fit_tup in fit_dict:
        if fit_tup[0] == fit_tup[1]: continue
        with open(fit_dict[fit_tup], 'r') as fh:
            fit_txt = fh.read()
        fit_txt_sub = re.search('(?<=fitting results \(bootstrap\):)[\sa-zA-Z.,0-9_+-:\(\)]+', fit_txt).group(0).strip().replace('\n\t', '\n').replace(':', '')
        fit_df = pd.read_csv(StringIO(fit_txt_sub), sep='\t').set_index('parameter')
        if exp == '4_kinetic_fingerprint':
            dt_df.loc[tuple(list(fit_tup) + [0])] = list(fit_df.loc['b_1(s)', :])
            dt_df.loc[tuple(list(fit_tup) + [1])] = list(fit_df.loc['b_2(s)', :])
        else:
            dt_df.loc[fit_tup] = list(fit_df.loc['b_1(s)', :])

    trdf = pd.DataFrame(index=dt_df.index, columns=['tr', 'ci_low', 'ci_high'])
    trdf.loc[:, 'tr'] = 1 / dt_df.loc[:, 'mean']
    trdf.loc[:, 'ci_high'] = 1 / (dt_df.loc[:, 'mean'] - dt_df.loc[:, 'sd']) - trdf.tr
    trdf.loc[:, 'ci_low'] = trdf.tr - 1 / (dt_df.loc[:, 'mean'] + dt_df.loc[:, 'sd'])

    if exp == '4_kinetic_fingerprint':
        trdf.reset_index(2, inplace=True)
        trdf.rename({'level_2': 'b'}, inplace=True, axis=1)
        departure_state = np.array(list(trdf.index))[:, 0]
        trdf.loc[:, 'bmod'] = departure_state == departure_state.min()
        trdf.loc[trdf.bmod, 'transition'] = trdf.loc[trdf.bmod, :].apply(lambda x: f'{efret2num_dict[x.name[0]]}_{int(efret2num_dict[x.name[1]] + x.b)}', axis=1).to_list()
        trdf.loc[np.invert(trdf.bmod), 'transition'] = trdf.loc[np.invert(trdf.bmod), :].apply(lambda x: f'{int(efret2num_dict[x.name[0]] + x.b) }_{efret2num_dict[x.name[1]]}', axis=1).to_list()
    else:
        trdf.loc[:, 'transition'] = trdf.apply(lambda x: f'{efret2num_dict[x.name[0]]}_{efret2num_dict[x.name[1]]}', axis=1).to_list()
    trdf.loc[:, 'tool'] = 'MASH-FRET'
    trdf.reset_index(drop=True, inplace=True)

    # parse efret states from stasi results
    trace_list = []
    sl_list = []
    for fn in parse_input_path(f'{in_dir}/traces_ASCII', pattern='*.txt'):
        tr = pd.read_csv(fn, sep='\t', usecols=['FRET_1>2', 'discr.FRET_1>2'])
        tr.rename({'FRET_1>2': 'efret', 'discr.FRET_1>2': 'ed'}, axis=1, inplace=True)
        state_levels = tr.ed.unique()
        sl_list.append(state_levels)
        if exp != '3_tandem_10nt':
            state_levels.sort()
            st_dict = {sl:si + 1 for si, sl in enumerate(state_levels)}
            tr.loc[:,'state'] = tr.ed.apply(lambda x: st_dict[x])
            tr.drop('ed', axis=1, inplace=True)
        trace_list.append(tr)
    efdf = pd.concat(trace_list)
    if exp == '3_tandem_10nt':
        sl_list = np.concatenate(sl_list).reshape(-1, 1)
        km = KMeans(n_clusters=max(soi)).fit(sl_list)
        ef2state_dict = {ef: lab + 1 for ef, lab in zip(list(sl_list.reshape(-1)), list(km.labels_))}
        efdf.loc[:, 'state'] = efdf.ed.apply(lambda x: ef2state_dict[x])
    efdf = efdf.query(f'state in {str(soi)}').copy()
    efdf.loc[:, 'tool'] = 'MASH-FRET'
    return efdf, trdf

# def parse_deepfret_results(ed, framerate):
#     with open()
#
#     return efdf, trdf

parser = argparse.ArgumentParser(description='Plot performance of different tools for side-by-side comparison')
parser.add_argument('--eval-dir', type=str, required=True)
parser.add_argument('--experiments', type=str, nargs='+', required=True,
                    help='String to identify directories for the same dataset analyzed by different tools')
parser.add_argument('--framerate', type=float, default=10.0,
                    help='recording frame raterequired to translate transition probs to transition rates')
parser.add_argument('--data-dir', type=str, required=True)
parser.add_argument('--out-dir', type=str, required=True)
args = parser.parse_args()

out_dir = parse_output_dir(args.out_dir)

# Collect states of interest
soi_dict = {}
for exp in args.experiments:
    with open(f'{args.data_dir}/{exp}/target_states.txt') as fh:
        soi_dict[exp] = [int(s) for s in fh.read().strip().split(' ')]

eval_dirs = [ed[0] for ed in os.walk(args.eval_dir)]
trdf_dict = {}
efret_df_dict = {}
efret_dict = {}
exp_dict = {}
for en in args.experiments:
    exp_dict[en] = [ed for ed in eval_dirs if en in basename(ed)]
    trdf_dict[en] = []
    efret_df_dict[en] = []
    efret_dict[en] = {}

# Collect E_FRET mean,sd, transition rates
tool_list = []
for exp in exp_dict:
    for ed in exp_dict[exp]:
        if ed.endswith('_eval'):  # the fretboard dir
            efdf, trdf = parse_fretboard_results(ed, soi_dict[exp])
        elif ed.endswith('_mash'):
            # if exp == '3_tandem_10nt':
            #     continue # Analysis failed
            efdf, trdf = parse_mashfret_results(ed, soi_dict[exp])
        elif ed.endswith('_ebFRET'):
            if exp != '1_2_state_donor_immobilized':
                continue  # Analyses failed
            efdf, trdf = parse_ebfret_results(ed, args.framerate, soi_dict[exp])
        # elif ed.endswith('_DeepFRET'):
        #     efdf, trdf = parse_deepfret_results(ed, args.framerate)
        else:
            continue
        tool_list.append(trdf.loc[0, 'tool'])
        trdf_dict[exp].append(trdf)
        efret_df_dict[exp].append(efdf)
tool_list = set(tool_list)

# --- plotting ---
color_dict = {tool: colors[ti] for ti, tool in enumerate(tool_list)}
nb_exp = len(exp_dict)
fig = plt.figure(constrained_layout=False, figsize=(48/2.54, 40/2.54))
gs = gridspec.GridSpec(2, nb_exp, figure=fig, wspace=0.6, hspace=0.30)
plot_types = ['efret', 'transition']

for cidx, exp in enumerate(exp_dict):
    if cidx == 0:
        ax_dict = {plot_name: fig.add_subplot(gs[idx, cidx]) for idx, plot_name in enumerate(plot_types)}
        first_ax_dict = ax_dict
    else:
        ax_dict = {plot_name: fig.add_subplot(gs[idx, cidx], sharey=first_ax_dict[plot_name]) for
                   idx, plot_name in enumerate(plot_types)}

    # move fb and manual results to back of list
    idx_fb = np.argwhere([edf.tool.iloc[0] == 'FRETboard' for edf in efret_df_dict[exp]])[0, 0]
    efret_df_dict[exp].append(efret_df_dict[exp].pop(idx_fb))
    trdf_dict[exp].append(trdf_dict[exp].pop(idx_fb))

    # Plot histograms of FRET values
    nb_states = len(efret_df_dict[exp][0].state.unique())
    plt.figure()
    fig_hists = plt.figure(constrained_layout=False, figsize=(48/2.54, 40/2.54))
    gs_hists = gridspec.GridSpec(nb_states, 1, figure=fig_hists,
                                 hspace=0.30)
    ax_list = [fig_hists.add_subplot(gs_hists[ii, 0]) for ii in range(nb_states)]
    plot_efret_dists(pd.concat(efret_df_dict[exp]), ax_list, color_dict, 'hist')
    fig_hists.savefig(f'{out_dir}efret_hists_{exp}.svg')

    # add plots to composition figures
    plot_efret_dists(pd.concat(efret_df_dict[exp]), ax_dict['efret'], color_dict, 'box')
    plot_tool_tr_bars(pd.concat(trdf_dict[exp]), ax_dict['transition'], color_dict)

    ax_dict['efret'].legend().remove()
    # if cidx == 0:
    #     ax_dict['efret'].legend(loc='upper center', #bbox_to_anchor=(0,5, 0.5, 0.5)
    #                             )
    # else:
    #     ax_dict['efret'].legend().remove()

fig.savefig(f'{out_dir}tool_comparison_composed.svg')
