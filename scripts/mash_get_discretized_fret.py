import os, sys, argparse
from io import StringIO
from os.path import splitext, basename
import pandas as pd
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(__location__)
from helper_functions import parse_input_path, parse_output_dir

parser = argparse.ArgumentParser(description='Calculate discretized fret from discretized don/acc')
parser.add_argument('--ascii-dir', type=str, required=True)
parser.add_argument('--out-dir', type=str, required=True)
args = parser.parse_args()

out_dir = parse_output_dir(args.out_dir)
trace_fn_list = parse_input_path(args.ascii_dir, pattern='*.txt')

for tr_fn in trace_fn_list:
    new_fn = out_dir + basename(tr_fn)
    with open(tr_fn, 'r') as fh:
        tr_txt = fh.read()
    tr_txt = tr_txt.replace('\t\n', '\n')
    trace_df = pd.read_csv(StringIO(tr_txt), sep='\t', index_col=None, )
    trace_df.loc[:, 'FRET_1>2'] = trace_df.loc[:, 'I_2 at 376nm(counts)'] / (trace_df.loc[:, 'I_1 at 376nm(counts)'] + trace_df.loc[:, 'I_2 at 376nm(counts)'])
    trace_df.loc[:, 'discr.FRET_1>2'] = trace_df.loc[:, 'discr.I_2 at 376nm(counts)'] / (trace_df.loc[:, 'discr.I_1 at 376nm(counts)'] + trace_df.loc[:, 'discr.I_2 at 376nm(counts)'])
    trace_df = trace_df.loc[:, ('time at 376nm', 'I_1 at 376nm(counts)', 'I_2 at 376nm(counts)', 'FRET_1>2', 'discr.FRET_1>2')]
    trace_df.to_csv(new_fn, sep='\t', index=False)
