import argparse, os, sys, re
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


parser = argparse.ArgumentParser(description='draw histogram of logps of an index table')
parser.add_argument('--in-csv', type=str, required=True)
parser.add_argument('--out-svg', type=str, required=True)
args = parser.parse_args()

df = pd.read_csv(args.in_csv)
df.loc[:, 'nb_states'] = df.apply(lambda x: [2, 3][int(re.search('(?<=trace_)[0-9]+(?=.dat)', x.trace).group(0))> 150], axis=1)

fig, ax = plt.subplots(figsize=(8.25, 2.9375))
sns.histplot(x='logprob', hue='nb_states', data=df, ax=ax, bins=30)
ax.set_xlabel('log(P($X_n$|θ)) / $T_n$')
plt.tight_layout()
plt.savefig(args.out_svg)
