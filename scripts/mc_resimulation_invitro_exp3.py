import os, sys
import pomegranate as pg
import numpy as np
import matplotlib.pyplot as plt

# Resimulate state paths for in vitro experiment 3 (tandem 10nt, 1 ground state, 2 FRET states),
# with separate chains for each FRET chain. This shows that co-occurence of FRET states
# is unlikely.

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(__location__)

from helper_functions import parse_input_path

def get_mc_sample(tr, st_dict, length):
    ps = pg.DiscreteDistribution(st_dict)
    cpt = pg.ConditionalProbabilityTable(tr,[ps])
    mc = pg.MarkovChain([ps, cpt])
    return np.array(mc.sample(length))

# Collect trace durations
trace_durations = []
trace_list = parse_input_path(f'{__location__}/../in_vitro/data/3_tandem_10nt/dats_unlabeled/', pattern='*.dat')
for trace_fn in trace_list:
    with open(trace_fn, 'r') as fh: lc = len(fh.readlines())
    trace_durations.append(lc)

# Define MC starting properties and transition rates, taken from invitro/eval/3_tandem_10nt_fb/FRETboard_report.html
st_dict = {'g': 1.0, 'f': 0.0}
tr_state2 = [
            ['g', 'g', 0.9997],
            ['g', 'f', 0.0003],
            ['f', 'g', 0.0535],
            ['f', 'f', 0.9465]]

tr_state3 = [
            ['g', 'g', 0.9995],
            ['g', 'f', 0.0005],
            ['f', 'g', 0.0625],
            ['f', 'f', 0.9375]]

simultaneous_list = []
for td in trace_durations:
    state2_seq = get_mc_sample(tr_state2, st_dict, td)
    state3_seq = get_mc_sample(tr_state3, st_dict, td)
    simultaneous_list.append(np.sum(np.logical_and(tr_state2 == 'f', tr_state3 == 'f')) / td)
plt.boxplot(simultaneous_list)
plt.show()
