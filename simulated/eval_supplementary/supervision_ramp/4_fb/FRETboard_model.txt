GmmHmm
START_NEW_SECTION
class: HiddenMarkovModel
distribution ties: []
edges:
- - 9
  - 6
  - 0.25000000000000006
  - 0.25
  - null
- - 9
  - 7
  - 0.25000000000000006
  - 0.25
  - null
- - 9
  - 8
  - 0.5
  - 0.5
  - null
- - 6
  - 10
  - 0.0
  - 0.0
  - null
- - 6
  - 6
  - 0.9833089922804089
  - 0.9833089922804089
  - null
- - 6
  - 0
  - 0.007302315877321096
  - 0.007302315877321094
  - null
- - 6
  - 1
  - 0.009388691842269977
  - 0.009388691842269977
  - null
- - 7
  - 10
  - 0.0005349023803155927
  - 0.0005349023803155924
  - null
- - 7
  - 7
  - 0.9903717571543194
  - 0.9903717571543194
  - null
- - 7
  - 2
  - 0.009093340465365075
  - 0.009093340465365071
  - null
- - 7
  - 3
  - 0.0
  - 0.0
  - null
- - 8
  - 10
  - 0.0003410641200545704
  - 0.00034106412005457026
  - null
- - 8
  - 8
  - 0.9919849931787176
  - 0.9919849931787176
  - null
- - 8
  - 4
  - 0.0076739427012278314
  - 0.007673942701227831
  - null
- - 8
  - 5
  - 0.0
  - 0.0
  - null
- - 0
  - 7
  - 1.0
  - 9999999
  - null
- - 1
  - 8
  - 1.0
  - 9999999
  - null
- - 2
  - 6
  - 1.0
  - 9999999
  - null
- - 3
  - 8
  - 1.0
  - 9999999
  - null
- - 4
  - 6
  - 1.0
  - 9999999
  - null
- - 5
  - 7
  - 1.0
  - 9999999
  - null
end:
  class: State
  distribution: null
  name: None-end
  weight: 1.0
end_index: 10
name: None
silent_index: 9
start:
  class: State
  distribution: null
  name: None-start
  weight: 1.0
start_index: 9
states:
- class: State
  distribution:
    class: Distribution
    frozen: false
    name: IndependentComponentsDistribution
    parameters:
    - - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.28645771999642927
        - 0.14330555796646727
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 25.245715053714285
        - 6.773237215500851
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 5.570948807222716
        - 1.3984729560320142
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - -0.07489569104223229
        - 0.38531727445245667
    - - 1.0
      - 1.0
      - 1.0
      - 1.0
  name: e0_1_0
  weight: 1.0
- class: State
  distribution:
    class: Distribution
    frozen: false
    name: IndependentComponentsDistribution
    parameters:
    - - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.4516982807494932
        - 0.1851457880461302
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 21.868398292
        - 5.09700660522921
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 4.609738522560018
        - 1.5303949677030269
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - -0.6219002449174232
        - 0.24514868035889043
    - - 1.0
      - 1.0
      - 1.0
      - 1.0
  name: e0_2_0
  weight: 1.0
- class: State
  distribution:
    class: Distribution
    frozen: false
    name: IndependentComponentsDistribution
    parameters:
    - - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.30682487880561576
        - 0.11750921852288926
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 25.98612611764706
        - 8.255453213969302
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 5.673247288534462
        - 1.7854656807779774
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - -0.1267837176434215
        - 0.4197552317570491
    - - 1.0
      - 1.0
      - 1.0
      - 1.0
  name: e1_0_0
  weight: 1.0
- class: State
  distribution:
    class: Distribution
    frozen: false
    name: IndependentComponentsDistribution
    parameters:
    - - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
    - - 1.0
      - 1.0
      - 1.0
      - 1.0
  name: e1_2_0
  weight: 1.0
- class: State
  distribution:
    class: Distribution
    frozen: false
    name: IndependentComponentsDistribution
    parameters:
    - - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.40277668555760393
        - 0.17905234401412481
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 25.634802240888895
        - 6.351734995493455
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 5.660825218261629
        - 1.7583953651732693
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - -0.4489570856985313
        - 0.4186435071466653
    - - 1.0
      - 1.0
      - 1.0
      - 1.0
  name: e2_0_0
  weight: 1.0
- class: State
  distribution:
    class: Distribution
    frozen: false
    name: IndependentComponentsDistribution
    parameters:
    - - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
      - class: Distribution
        frozen: false
        name: NormalDistribution
        parameters:
        - 0.0
        - 999999.0
    - - 1.0
      - 1.0
      - 1.0
      - 1.0
  name: e2_1_0
  weight: 1.0
- class: State
  distribution:
    class: GeneralMixtureModel
    distributions:
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.16614158880240246
          - 0.13885141472810866
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 24.148665060750016
          - 4.5668107212784195
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 2.585121652558863
          - 0.8622697964919318
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - -0.7053926921772785
          - 0.15830700196130665
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.1829694490383053
          - 0.16660865318842152
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 19.741619709824104
          - 3.8640477092457743
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 4.785889648856351
          - 1.3073191708009748
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - -0.05519667694472404
          - 0.3499542445350076
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.2209508835596468
          - 0.07265440722536604
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 29.88563963638847
          - 6.591109227622488
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 6.454043509410041
          - 1.5349092032504632
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.4265151590867706
          - 0.3040239191743424
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.1415601931448549
          - 0.153915353492261
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 17.282012217420217
          - 4.582812589076596
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 6.924382936207628
          - 1.8658641323734864
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.56650361377622
          - 0.21847682025253237
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.21179637312373542
          - 0.08280565612542057
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 28.9746301820851
          - 4.233110362643825
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 4.386835404743626
          - 1.072984846694664
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - -0.17991472530990107
          - 0.33029020570064105
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    weights:
    - 0.15707060138706827
    - 0.24520522871591816
    - 0.22863439420095882
    - 0.10863017576359044
    - 0.2604595999324643
  name: s0
  weight: 1.0
- class: State
  distribution:
    class: GeneralMixtureModel
    distributions:
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.3889184232210501
          - 0.07680225272090654
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 25.65303964577754
          - 8.60654143620812
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 6.8107937994498995
          - 1.9643516186878598
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.6714835992930228
          - 0.1915220827240973
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.34420388430961385
          - 0.2523279475477635
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 14.85490438295487
          - 3.16022557062324
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 5.9930337513042495
          - 1.2569683768865332
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.23108152963598974
          - 0.3970958328513425
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.36547408759242994
          - 0.14730695885389503
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 23.47609713527367
          - 4.096294603276802
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 2.2727757846143604
          - 0.6944050614292061
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - -0.7711609471331001
          - 0.12042242605108178
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.36957256828865165
          - 0.13955854368992024
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 24.08440861725623
          - 5.097383653188848
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 3.848442872553783
          - 0.9155495966550892
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - -0.33252779755303336
          - 0.21006077576704169
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.3593652056316898
          - 0.1030321904588981
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 25.97764246285598
          - 5.650574998221888
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 5.538749946754697
          - 1.2124815146861425
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.18151202732232147
          - 0.28023291685681095
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    weights:
    - 0.16547862013486708
    - 0.07248667329970465
    - 0.10062094928473729
    - 0.31650450312756145
    - 0.34490925415312956
  name: s1
  weight: 1.0
- class: State
  distribution:
    class: GeneralMixtureModel
    distributions:
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.5802246754515152
          - 0.13957194570356302
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 24.166388673120885
          - 5.015683668194591
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 4.274827440415178
          - 0.8889088439766473
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - -0.20055847811202707
          - 0.28588567813202204
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.5999636650894763
          - 0.08813324803346588
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 28.73562424106759
          - 5.644093842047099
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 6.209695726736672
          - 1.4107756133731035
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.39693420838279314
          - 0.32887547172753273
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.6047278023347005
          - 0.1693050977407665
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 24.207154340742264
          - 3.986058977201619
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 2.7417158649195374
          - 0.8095976825887881
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - -0.6678565652719933
          - 0.1758108716917942
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    - class: Distribution
      frozen: false
      name: IndependentComponentsDistribution
      parameters:
      - - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.617499177237839
          - 0.1670258392899461
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 18.566841789556737
          - 4.6041436290199265
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 5.91663406061914
          - 1.5376982406382325
        - class: Distribution
          frozen: false
          name: NormalDistribution
          parameters:
          - 0.43658871501443364
          - 0.29161728543460536
      - - 1.0
        - 1.0
        - 1.0
        - 1.0
    weights:
    - 0.3678002191009396
    - 0.28712678508237166
    - 0.17483661289421892
    - 0.17023638292246984
  name: s2
  weight: 1.0
- class: State
  distribution: null
  name: None-start
  weight: 1.0
- class: State
  distribution: null
  name: None-end
  weight: 1.0

START_NEW_SECTION
E_FRET
i_sum
i_sum_sd
correlation_coefficient
START_NEW_SECTION
0: 1
1: 2
2: 0
3: 2
4: 0
5: 1
6: 0
7: 1
8: 2
9: null
10: null

START_NEW_SECTION
e0_1_0: 1
e0_2_0: 2
e1_0_0: 0
e1_2_0: 2
e2_0_0: 0
e2_1_0: 1
s0: 0
s1: 1
s2: 2

START_NEW_SECTION
None-end: 10
None-start: 9
e0_1_0: 0
e0_2_0: 1
e1_0_0: 2
e1_2_0: 3
e2_0_0: 4
e2_1_0: 5
s0: 6
s1: 7
s2: 8

START_NEW_SECTION
nb_states: 3
buffer: 1
dbscan_epsilon: nan